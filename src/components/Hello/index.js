// @flow strict
import { OspElement, html } from 'osp-lit-common';
import css from './style.pcss';
import tpl from './template';

const MAX_ENTHUSIASM = 5;
const ENTHUSIASM_INTERVAL_MS = 1000;

export default class Hello extends OspElement {
	/**
	 * This defines the class properties which the component reacts to, triggering template updates.
	 *
	 * There are some other behaviours you can include in addition to the property types, but they're
	 * seldom needed: see https://github.com/Polymer/lit-element
	 *
	 * OspElement provides an additional option, `default`, which does what you would guess. For Object
	 * and Array properties, you should probably put the value in an arrow function (like, to initialize
	 * an empty array: 'default': () => []). Otherwise, all elements of the same type will share a
	 * reference to the same object.
	 *
	 * There are a few extra options meant to work with the documentation system as well:
	 *  * `description`: string describing what the property means
	 *  * `private`: boolean describing whether to include the property in the doc, default true
	 * 		`private` is useful when a property is listed only because rendering dependds on it, but
	 * 		it isn't any concern to outside users. Like `enthusiam` here, which is internally managed
	 */
	static get properties() {
		return {
			'name': {
				'description': 'Who to greet',
				'type': String,
				'default': 'world'
			},
			'enthusiasm': {
				'type': Number,
				'private': true,
				'default': 1
			}
		};
	}

	// speaking of that documentation system, a few more things you can add:

	/**
	 * What do
	 */
	static get description() {
		return 'Venerable/cliché "Hello World" component';
	}

	/**
	 * How to react to this component.
	 *  {
	 *    'event type': {
	 *      'field in event detail': 'description of meaning',
	 *      ...
	 *    }
	 *  }
	 */
	static get events() {
		return {
			'change': {
				'name': 'Who\'s being greeted',
				'enthusiasm': 'How much they\'re being greeted'
			}
		};
	}


	constructor() {
		super();

		setInterval(
			() => this.enthusiasm = this.enthusiasm === MAX_ENTHUSIASM ? 1 : this.enthusiasm + 1,
			ENTHUSIASM_INTERVAL_MS
		);
	}

	/**
	 * Elements that have state that might be interesting to outside observers should encode
	 * it into a JSON-encodable structure and return it from the value getter
	 *
	 * The parent element can then make use of it in different contexts automatically, chief
	 * among them being Jupyter contexts where the value can be stored to and restored from
	 * the Python context in the document, enabling components to work together with those
	 * that are coded in Python
	 */
	get value() {
		return {
			'enthusiasm': this.enthusiasm,
			'name': this.name
		};
	}

	/**
	 * If you need to do something in response to a property change (observers, computed fields, etc)
	 * you can do so here.
	 *
	 * There is an equivalent firstUpdated(fields) that does the same thing but only fires once after
	 * the DOM is ready. This can be useful for things like D3 that want to do one-time initialization
	 * of the document.
	 */
	updated(fields: Map<string, ?mixed>) {
		if (fields.has('name')) {
			this.name = `${ this.name[0].toUpperCase() }${ this.name.slice(1).toLowerCase() }`;
		}
		// emit events about about state changes in case anyone's interested.
		// you can supply the detail of the event here, or leave it blank to defer to this.value
		this.signalChange();
	}

	render() {
		return html`<style>${ css }</style>${ tpl(this) }`;
	}
}
customElements.define('osp-hello', Hello);
