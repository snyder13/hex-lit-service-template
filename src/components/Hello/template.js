// @flow strict
import { html } from 'osp-lit-common';
import type Hello from './index';

/**
 * lit-html is a declarative template engine.
 *
 * Usage, quoth the docs: https://github.com/Polymer/lit-element
 *
 *	static elements: html`<div>Hi</div>`
 * expression: html`<div>${this.disabled ? 'Off' : 'On'}</div>`
 * property: html`<x-foo .bar="${this.bar}"></x-foo>`
 * attribute: html`<div class="${this.color} special"></div>`
 * boolean attribute: html`<input type="checkbox" ?checked=${checked}>`
 * event handler: html`<button @click="${this._clickHandler}"></button>`
 *
 * Here is a good summary of how it works under the hood for the interested:
 * https://medium.com/@lucamezzalira/a-night-experimenting-with-lit-html-585a8c69892a
 */
export default (inst: Hello) => html`
	<p>Hello, <span>${ inst.name }</span>${ (new Array(inst.enthusiasm)).fill().join('!') }</p>
`;
