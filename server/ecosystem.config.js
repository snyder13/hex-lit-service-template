'use strict';
module.exports = {
	/**
	 * http://pm2.keymetrics.io/docs/usage/application-declaration/
	 */
	'apps' :[ {
		'name': 'profile-page',
		'script': `${ __dirname }/server.js`,
		'env': {
			'DEBUG': 'hex:*'
		},
		'env_production': {
			'NODE_ENV': 'production'
		}
	} ]
};
