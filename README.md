# hex-lit-service-template

This repo represents a starting point for a microservice that is implemented in ECMAScript front-to-back.

## Basic Usage

 1. Clone the repo
 2. Reset git and remove this file to make room for your own: `rm -fr .git README.md`
 3. Edit `package.json` to enter your package name and reset the version. Add `'private': true` if you don't intend to publish to the NPM registry.
 4. Initialize a new repo: `git init .`, `git add * */.*`, `git commit -m "initial commit"`. You most likely want to set up a remote as well.
 5. Install deps: `yarn install` or `npm install`
 6. Launch dev server: `yarn start` or `npm start`. This'll cause some unexciting proof of life to appear at http://localhost:8080.

## Contents

### lit-element

Lit is the new development path prescribed by the Polymer project for projects implementing web components.

Polymer (the library) still exists as well under the umbrella of the Polymer project, but it kind of ran astray of its design goal of being (only) a polyfill for standard-track technology, with the hope that its functionality will one day land in browsers and just be The Way Things Work (tm).

Polymer the library added a lot of convenience utilities and bells and whistles above and beyond what can practically be specced out for a standard. Lit is a response to that feature-creep, implementing a much more stripped-down API re-aimed at coinciding with standards.

Thanks to this small surface area, the documentation fits neatly into the [project README](https://github.com/Polymer/lit-element).

You can generate a new component from the shell defined in `skeleton/template/*` by running `node skeleton/create Your/Component/Path`. Components should be prefixed with something in the tag name. By default this would turn into `<osp-your-component-path>`. You can pass `--prefix=foo` for `<foo-your-component-path>`.

The folder structure is up to you and the scope of your app, whether you go with a largely flat organization or section things off into folders by some logic. `Component/Path` and `ComponentPath` both map to `<osp-component-path>`, so just mind conflicts of that nature.

#### Web components

Web components are a way to package functionality into tags that you can add to HTML repertoire for your application. Each tag is represented by a ES6 class inheriting from `HTMLElement`. So, they behave like standard elements: they have attributes you can set to customize its behavior, and they emit events like `onChange` to notify interested parties about their state.

Speaking of state, the inner workings of a web component are encapsulate in a private DOM area called the shadow DOM. :spooky:

In the shadow DOM, nothing permeates the barrier except for attributes and events. You can treat each component as being its own micro-application. Since the scope is contained, if you have one `<input>` in your component you can go ahead and style it with `input { ... }`, and you can give things very generic sounding `id` attributes like `"submit"` without worry.

Your component is the only thing that can step on its feet, so in true microservice fashion you can focus on the exact minimal problem at hand.

Since web components follow the conventions of any other tag, they can fit into other framework just as naturally as, say `<input />`. This is good because nobody can agree on anything. Whether it's "vanilla JS" or React or more Lit or jQuery or so on ad infinitum, working with web components boils down to managing markup and events, and that's what all these tools are designed for.

So, web components have great portability between different contexts, which is a clear advantage given the "stack diversity" inevitably introduced by a microservice architecture.

Inside the web component, there is a template that has reactive updates similar to Vue or React, but again with a stripped-down API and an eye toward standards. Templates are implemented using [polyfills of] HTML5's [`<template>` tag](https://html.spec.whatwg.org/multipage/scripting.html#the-template-element), which imparts some [mildly interesting benefits](https://lucamezzalira.com/2018/08/14/a-night-experimenting-with-lit-html/).

### Express/hex

Express is a middleware stack for server-side ECMAScript that is used by the vast majority of projects in that space.

The idea is that you define bits of your application as mutators of the request and response state, and they're called sequentially to compose all their functionality into a beautiful and functional final response.

Silly example:
```javascript
app.get('/admin', (req, res, next) => {
	if (req.headers.authorization !== 'secret') {
		return res.status(405).type('json').send({ 'error': 'no way' });
	}
	req.locals.access = 'boss';
	next();
});
app.get('/admin', (req, res) => {
	res.render('admin', { 'username': req.locals.access });
});
```

So, middleware examine aspects of `req` and either make a response or annotate the request for future middleware. If none ever take final responsibility to `send()` anything you get a 404.

Again, this coincides nicely with the microservice mindset because you can do only what you need to do to achieve some very-specific goal for the request in one middleware.

In this repo, Express is used as the router, landing a web component that can vary depending on the URL. Alternatively, you may prefer something like [lit-router](https://github.com/danevans/lit-router) to do client-side routing.

#### hex

Hex is a small library built on top of Express that addresses the difficulty of composing a large number of middleware into a cohesive whole.

Express works on a first-bound first-called basis, so if the two middleware above were swapped there would be a problem. The rendering middleware would be called regardless of whether it passed the check for the header, and the username would be undefined in the `admin` view.

This is easy to avoid when the middleware are in the same file, but presumably you'd like to structure your application more sensibly than putting everything in one file, and you'd like to abstract common middleware to libraries where multiple applications can take advantage of them.

To that end, hex adds a file `middleware.js` where you can specify a dependency tree. Assuming the first middleware is moved to a library `my-auth` and named `check-secret`, and the second is still in your application as `render-admin`, you could define their relationship in `middleware.js` as:

```javascript
module.exports = {
	'render-admin': {
		'description': 'Show the admin page',
		'deps': [ 'my-auth.check-secret' ]
	}
};
```

When hex starts it will resolve all these dependencies and load everything in the proper order. This is a trivial example but when you have deps of deps of deps of etc it's very helpful for separation of concerns to determine their load order automatically.

Hex adds a few other things you can read about [at the project page](https://bitbucket.org/snyder13/hex).

In short, it helps to publish static assets by serving anything in a path named `public/` in your repository relative to the server root, and it provides `conf.js` and `secrets.js`, two files that contain configuration options relevant to the operation of all the middleware depended on by your application. It also provides a migration utility to pull in schema from all the middleware you use.

There are several common middleware published for hex, which you can see primarily at https://bitbucket.org/snyder13 and at the HUBzero GitLab. These provide things like session management, database access, and template engines.

For example, the included server uses `hex-views-lit-html`, a middleware which configures Express to render things in the `views/` path with `lit-html` so that they match up with templates on the client side.

### Webpack, Babel & PostCSS

The stalwart frenemy of all modern JS developers, Webpack and Babel must make an appearance here to allow us to write new (and even future) dialects of ES6 that still work in living browsers.

The configuration shipped is such that we can use the majority of ES6 features and the bundle works for IE11 and real browsers with more than 0.25% share.

If you find syntax you like missing there's most likely something you can add to `babel.config.js` to support it.

PostCSS is basically Babel for stylesheets, adding polyfilled support for [stage-2 CSS future recommendations](https://preset-env.cssdb.org/features#stage-2) and automatic vendor prefixing so you can stick to just base property names in your source. The [`postcss-preset-env` docs ](https://github.com/csstools/postcss-preset-env) show how to configure different handling of CSS future features, and the [PostCSS docs](https://github.com/postcss/postcss) show some additional transformation possibilities.

This repo also ships PostCSS plugins to support `@import`ing local files, define variables, and the `color()` function to make derivatives of a base color.

Examples of that stuff:

```css
@import './base-style.pcss';

:root {
	--my-color: #36a383;
}

#some-element {
	background: var(--my-color);
	color: color(var(--my-color) lightness(90%));
}
```

#### A note about module types

The `src/` path uses ES6 modules, which look like `import ... from '...'`. The `server/` path uses CommonJS modules, which look like `const .... = require('...')`. This is because at the time of writing the version of Node packaged in things like the `pm2` Docker do not support the former idiom. Both paths will use ES6 modules at some point.

### Linting and Types

This repo ships `eslint` and `stylelint` for code and style respectively, along with some sane-ish defaults.

You are encouraged to change `.eslintrc.js` and `.stylelintrc` to conform to your expectations rather than "fixing" errors they report you do not feel are errors. The idea is that each repo should be internally-consistent but people who own features have styling right of way. This is a minor inconvenience when visiting another repo but easier than finally settling tabs v. spaces.

`yarn lint` also type-checks your project according to Facebook's [Flow](https://flow.org/).

#### Flow
Flow is very similar to TypeScript, except:

1. It's opt-in. All TypeScript files must pass static analysis to be transpiled, but Flow only checks files with `// @flow` or `// @flow strict` annotations at the top. This means Flow cannot break your build.
2. Flow has a more rigorous & sound type system. This means that it can catch more errors at static analysis than TypeScript can. However, it can also be more difficult to type dynamic interactions. (`// $FlowFixMe` can ignore lines.)

In general static typing is an awkward fit for ECMAScript, but having used it I can testify that it does catch many logical errors that would be difficult to discover otherwise.

You can decide when/if to opt in to type-checking. It's more important for foundational libraries than for individual applications where edge-case errors may be negligible.

### Testing

Run `yarn test` to run tests.

If tests fail, you can get a lot more information by connecting to the `karma` debug server. Run `yarn test-debug`, then navigate a browser to `http://localhost:9876/debug.html` and check your browser's inspector panel.

## Scripts

Use `npm` instead of `yarn` if you'd like.

* `yarn install` - Install dependencies. Run after cloning.
* `yarn start` - Runs a hot-reloading dev server. Navigate to `http://localhost:8080`. It'll rebuild and reload when you make changes in `src/`. `*.html` from `src/` are copied, so you can use any number of those to test interactions of different components.
* `yarn lint` - Run various QA utilities per above
* `yarn build` - Bundle the client JS into `dist/`
* `yarn watch` - Wait for changes in `src/` and build into `dist/`
* `yarn server` - Starts the hex server in the `server/` path, loading whatever middleware you have defined for it.
* `yarn deploy` - Copy `dist/` into `server/public/`. This is necessary for the `yarn server` path. All `.html` files are omitted, as your server is expected to serve its own `views/` for a given request. At some point it may make sense to edit `package.json` so `yarn server` does this automatically, or to change `webpack.config.js` so the output path is `server/public/` instead of `dist/` to begin with.
* `yarn migrate` - Run migrations for installed storage engines in your app and each dependency that can use that storage.
* `node skeleton/create Desired/New/Component/Path [--prefix=osp]` - Generates a new component shell in the desired path
