// @flow strict
import { html } from 'osp-lit-common';
import type CLASS_NAME from './index';

export default (inst: CLASS_NAME) => html`
	<p>Hello, <span>${ inst.name }</span></p>
`;
