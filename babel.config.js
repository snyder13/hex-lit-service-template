const env = {
	'prod': 'prod',
	'production': 'prod',
	'test': 'test',
	'testing': 'test'
}[process.env.NODE_ENV ? process.env.NODE_ENV.toLowerCase() : ''] || 'dev';

module.exports = {
	'presets': env === 'test' ? [] : [
		[ '@babel/preset-env', {
			'targets': '> 0.25%, not dead'
		} ],
		...(env === 'prod' ? [ 'minify' ] : [])
	],
	'plugins': [
		'babel-plugin-syntax-flow',
		'babel-plugin-transform-flow-strip-types',
		'@babel/plugin-transform-async-to-generator',
		'@babel/plugin-transform-arrow-functions',
		'@babel/plugin-proposal-object-rest-spread'
	]
};

