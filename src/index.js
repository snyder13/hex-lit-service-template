// @flow strict

// import all the components that are instantiated at the outer scope (in your .html or server-side template files)
// those components can import their own dependencies so they don't all have to be listed here
import './components/Hello';
