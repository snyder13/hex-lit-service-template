# 1.0.1

* Added a reference to the `osp-lit-common` bundle. This is used currently to show the OSP template, but should eventually have a lot common UI bits and pieces. This could also be accomplished by `import`-ing things from `osp-lit-common`, but with the optimistic view that the common elements will some day be used across the site, it's better to load it from one central bundle to take advantage of the client cache.
