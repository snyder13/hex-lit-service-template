module.exports = {
	'parser': 'babel-eslint',
	'parserOptions': {
		'sourceType': 'script'
	},
	'plugins': [ 'flowtype' ],
	'rules': {
		'strict': [ 'error', 'global' ]
	}
};
