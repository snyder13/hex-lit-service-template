'use strict';

module.exports = (html, { name }) => ({
	'layout': 'html',
	'title': 'hello',
	'body': html`<osp-hello name="${ name }"></osp-hello>`
});
