import { expect } from 'chai';
import Hello from '../src/components/Hello';
import axe from 'axe-core';

describe('Hello element', () => {
	const el = document.createElement('osp-hello');

	// accessibility check
	it('a11y', async () => {
		document.body.appendChild(el);
		expect((await axe.run(el)).violations).to.be.empty;
	});

	it('implements metadata for docgen', () => {
		expect(Object.entries(Hello.properties).filter(([ _, prop ]) => !prop.private && !prop.description)).to.be.empty;
		expect(Hello.description).not.to.be.empty;
		expect(Hello.events).to.be.an('object').and.not.empty;
	});

	it('default props', () =>
		expect(el.name).to.equal('World')
	);

	it('accepts provided name', (done) => {
		el.name = 'jen';
		// immediately after, value is the same
		expect(el.name).to.equal('jen');
		// but after microtiming the update() function is called to normalize
		setImmediate(() => {
			expect(el.name).to.equal('Jen')
			done();
		});
	});

	const verifyValue = (val) => {
		expect(val).to.have.all.keys('name', 'enthusiasm');
		expect(val.enthusiasm).to.be.a('number');
	};

	it('reflects state in .value', () =>
		verifyValue(el.value)
	);

	it('emits events reflecting state', (done) => {
		el.name = 'foo';
		el.addEventListener('change', ({ detail }) => {
			verifyValue(detail);
			expect(detail.name).to.equal('Foo');
			done();
		});
	});
});
