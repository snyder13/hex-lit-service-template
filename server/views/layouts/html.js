'use strict';

module.exports = (html, { body, title }) =>	html`<!DOCTYPE html>
<html>
<head>
	<script src="https://rmer.info/osp/bootstrap.common.js"></script>
	<script src="bootstrap.js"></script>
	<title>Test: ${ title }</title>
	<style>
/* generic centering, feel free to change/remove */
osp-template > * {
	grid-column: 3 / -3;
}
	</style>
</head>
<body>
	<osp-template>
		${ body }
	</osp-template>
</body>
</html>`;
