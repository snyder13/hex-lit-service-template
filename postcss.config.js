const easyImport = require('postcss-easy-import');
const presetEnv = require('postcss-preset-env');
const cssnano = require('cssnano');
const cssVariables = require('postcss-css-variables');
const colorFunction = require('postcss-color-function');

module.exports = {
	'plugins': [
		easyImport({
			'extensions': [
				'.pcss',
				'.css',
				'.postcss',
				'.sss'
			]
		}),
		cssVariables(),
		colorFunction(),
		presetEnv(),
		cssnano()
	]
};
