'use strict';

module.exports = ({ app, conf }) => {
	app.get('/', (req, res) => res.render('hello', { 'name': conf.get('defaultName') }));
};
