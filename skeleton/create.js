'use strict';
const { basename, resolve } = require('path');
const fs = require('fs');

let prefix = 'osp';
const [ path ] = process.argv.slice(2)
	.filter((arg) => {
		const ma = arg.match(/--prefix=(.*)/);
		if (ma) {
			prefix = ma[1];
			return false;
		}
		return true;
	});

if (!path) {
	console.log(`Usage: ${ basename(process.argv[1]) } Desired/Component/Path [--prefix=osp]`);
	process.exit();
}
const className = path.replace(/\//g, '');
const tagName = `${ prefix }${ className.replace(/([A-Z])/g, (ch) => `-${ ch.toLowerCase() }`) }`;
const base = resolve(`${ __dirname }/../src/components`);

const pathParts = path.split('/');
for (let idx = pathParts.length - 1; idx >= 0; --idx) {
	const dir = `${ base }/${ pathParts.slice(0, pathParts.length - idx).join('/') }`;
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}
}
const dest = `${ base }/${ path }`;
console.log(`${ tagName } in ${ dest }:`);
const work = [];
fs.readdirSync(`${ __dirname }/template`)
	.filter((name) => !(/^[.]/).test(name))
	.filter((name) => {
		if (fs.existsSync(`${ dest }/${ name }`)) {
			console.log(`\texists, skipped: ${ name }`);
			return false;
		}
		return true;
	})
	.forEach((name) => {
		console.log(`\t${ name }`);
		fs.writeFileSync(`${ dest }/${ name }`,
			fs.readFileSync(`${ __dirname }/template/${ name }`, { 'encoding': 'utf8' })
				.replace(/CLASS_NAME/g, className)
				.replace(/TAG_NAME/g, tagName)
		);
	})

