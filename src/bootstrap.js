// this isn't transpiled, so either set that up or use lowest-common-denominator olde style js

/* eslint-disable func-names */
/* eslint-disable prefer-arrow-callback */
/* eslint-disable prefer-template */
/* eslint-disable no-var */
/* eslint-disable vars-on-top */
'use strict'; // eslint-disable-line strict

function ready() {
	var base = null;
	var scripts = document.getElementsByTagName('script');
	for (var idx = 0; idx < scripts.length; ++idx) {
		if ((/\/bootstrap.js$/).test(scripts[idx].src)) {
			base = scripts[idx].src.replace(/bootstrap.js$/, '');
			break;
		}
	}

	if (!base) {
		console.error('failed to bootstrap: could not determine base URL'); // eslint-disable-line no-console
		return;
	}

	function writeScript(attrs) {
		var script = document.createElement('script');
		for (var key in attrs) {
			script.setAttribute(key, attrs[key]);
		}
		document.body.insertBefore(script, document.body.firstChild);
	}

	[
		'vendor/polyfill.min.js',
		'vendor/webcomponentsjs/custom-elements-es5-adapter.js',
		'vendor/webcomponentsjs/webcomponents-loader.js'
	].forEach(function(path) {
		writeScript({ 'src': base + path });
	});

	document.addEventListener('WebComponentsReady', function componentsReady() {
		document.removeEventListener('WebComponentsReady', componentsReady, false);

		writeScript({
			'src': base + 'bundle.js'
		});
	}, false);
}

if (document.readyState === 'complete' || document.readyState === 'interactive') {
	ready();
}
else {
	document.addEventListener('DOMContentLoaded', ready);
}

