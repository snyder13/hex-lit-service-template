const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const dist = path.resolve('./dist')

module.exports = {
	'mode': 'none',
	'entry': [ './src' ],
	'devtool': 'source-map',
	'output': {
		'path': dist,
		'filename': 'bundle.js'
	},
	'plugins': [
		new CopyWebpackPlugin([ {
			'from': 'assets/**',
			'context': path.resolve('./src'),
			'to': dist
		}, {
			'from': 'src/bootstrap.js',
			'to': dist
		}, {
			'from': '*.html',
			'context': path.resolve('./src'),
			'to': dist
		}, {
			'from': 'node_modules/@webcomponents/webcomponentsjs/*',
			'to': path.resolve(dist, 'vendor/webcomponentsjs'),
			'flatten': true
		}, {
			'from': 'node_modules/@babel/polyfill/dist/polyfill.min.js',
			'to': path.resolve(dist, 'vendor')
		}, {
			'from': 'node_modules/whatwg-fetch/dist/fetch.umd.js',
			'to': path.resolve(dist, 'vendor')
		}, {
			'from': 'node_modules/@webcomponents/webcomponentsjs/bundles/*',
			'to': path.resolve(dist, 'vendor/webcomponentsjs/bundles'),
			'flatten': true
		}
		])
	],
	'module': {
		'rules': [
			{
				'test': /[.]js$/,
				'use': [ 'babel-loader' ],
				// ordinarily omits node_modules/, but some stuff we use there needs to be transpiled as well
				'exclude': () => false
			},
			{
				'test': /\.p?css$/,
				'use': [ 'text-loader', 'postcss-loader' ]
			}
		]
	}
};
