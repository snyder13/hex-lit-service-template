// @flow strict
import { OspElement, html } from 'osp-lit-common';
import css from './style.pcss';
import tpl from './template';

export default class CLASS_NAME extends OspElement {
	static get properties() {
		return {
			'name': {
				'type': String
			}
		};
	}

	constructor() {
		super();
		this.name = 'world';
	}

	render() {
		return html`<style>${ css }</style>${ tpl(this) }`;
	}
}
customElements.define('TAG_NAME', CLASS_NAME);
